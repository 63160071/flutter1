// import 'package:flutter/material.dart';
// void main() => runApp(HelloFlutterApp());
// //safeArea Widget
// class HelloFlutterApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context){
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter adad"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(
//                 onPressed: () {},
//                 icon: Icon(Icons.refresh)
//             )
//           ],
//         ) ,
//         body: Center(
//           child: Text(
//             "Hello Flutterasdsadadd",
//             style: TextStyle(fontSize: 24),a
//           ),
//         ),
//       ),
//     );
//   }
// }

 import 'package:flutter/material.dart';
    void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() =>  _MyStatefulWidgetState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola flutter";
class _MyStatefulWidgetState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("ดุดันไม่เกรงใจใคร"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting ? spanishGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.refresh),


            ),IconButton(
              onPressed: () {
                setState(() {
                  displayText = "ดุดันไม่เกรงใจใคร";
                });
              },
              icon: Icon(Icons.add_card_sharp),


            ),IconButton(
              onPressed: () {
                setState(() {
                  displayText = "FordRenger Rapter Generation ใหม่";
                });
              },
              icon: Icon(Icons.pan_tool),


            )


          ],
        ) ,
        body: Center(
          child: Text(
            displayText,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}


